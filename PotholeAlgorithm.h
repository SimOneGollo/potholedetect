#pragma once
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include "Defect.h"
#include "Algorithm.h"

#include "utils.h"

using namespace cv;
using namespace std;


class PotholeAlgorithm : Pothole::Algorithm
{

public:
	PotholeResult run(InputArray image) override;
	Mat wrapFrame(InputArray image, Mat& H) override;

	PotholeParameters getParams() { return params; }
	void setParams(PotholeParameters params) { this->params = params; }
	void loadTestResult(vector<PotholeResult> results) { this->results = results; }

protected: 

private:
	vector<vector<Point2f>> extractContourCanny(InputArray image, OutputArray output);//done
	// parallelizable
	void trackPreviousDefect(Mat image); //done
	vector<Defect> trackNewDefect(InputArray image, InputArray mask, vector<Defect> newDefects, vector<Defect> oldDfects, int startTrack);

	vector<Defect> joinOverlapped(vector<Defect> defects, float overPercentage); //done
	bool filterDefect(InputArray mask, Defect defect); //done
	int getNewId();//done

	int lastId = 0;
	float sf = 0.5;

	vector<Defect> prevDefect;
	vector<Defect> outDefect;
	Mat kernel33 = Mat::ones(Size(3, 3), CV_8U);
	PotholeParameters params = PotholeParameters::getDefaultParams();

	vector<PotholeResult> results;
	int TP = 0, FP = 0, FN = 0;
};

