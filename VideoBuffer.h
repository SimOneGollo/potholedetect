#pragma once
#include <QThread>
#include <opencv2/core.hpp>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <QDebug>

using namespace cv;

class VideoBuffer : public QThread
{
	Q_OBJECT
signals:
	void newImage(Mat image);

private slots:
	void buffImage(Mat image);

public:
	VideoBuffer()
		: QThread()
	{
	};

	void retrieveFrame() {
		nextReq = true;
		conditionVariable.notify_one();
	};

private:
	void run() override;
	std::queue<Mat> videoBuffer;
	std::mutex buffMutex;
	std::condition_variable conditionVariable;
	bool nextReq = false;
};