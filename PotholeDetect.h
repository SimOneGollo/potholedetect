#pragma once

#include <QtWidgets/QMainWindow>
#include <QFileDialog>

#include "ui_PotholeDetect.h"
#include "FrameCapture.h"
#include "PotholeAlgorithm.h"
#include "GPUPotholeAlgorithm.h"
#include "TestManager.h"
#include <QSpinBox>
#include "VideoBuffer.h"
using namespace std;

class PotholeDetect : public QMainWindow
{
    Q_OBJECT
private slots:
    void openVideo();
    void openFromFile();
    void playPause();
    void previousFrame();
    void nextFrame();
    void UpdateImage(Mat img);

    void setCannyValue(int value) {
        PotholeParameters param = potAlg.getParams();
        param.cannyVal = value;
        potAlg.setParams(param);
        Gpu_potAlg.setParams(param);
    };
    void setCannyRateo(int value) {
        PotholeParameters param = potAlg.getParams();
        param.ratio = value;
        potAlg.setParams(param);
        Gpu_potAlg.setParams(param);
    };
    void setCannyKernelSize(int value) {
        PotholeParameters param = potAlg.getParams();
        param.kernelSize = value;
        potAlg.setParams(param);
        Gpu_potAlg.setParams(param);
    };

    void setGradientMinThr(int value) {
        PotholeParameters param = potAlg.getParams();
        param.gradientMinThr = value;
        qDebug() << value;
        potAlg.setParams(param);
        Gpu_potAlg.setParams(param);
    }
    void setGradientValue(double value) {
        PotholeParameters param = potAlg.getParams();
        param.gradientVal = value;
        qDebug() << value;
        potAlg.setParams(param);
        Gpu_potAlg.setParams(param);
    }

    void setOverlapRateo(double value) {
        PotholeParameters param = potAlg.getParams();
        param.overlappedRateo = value;
        qDebug() << value;
        potAlg.setParams(param);
        Gpu_potAlg.setParams(param);
    }
    void setIntersectRateo(double value) {
        PotholeParameters param = potAlg.getParams();
        param.intersectionRateo = value;
        qDebug() << value;
        potAlg.setParams(param);
        Gpu_potAlg.setParams(param);
    }

    void setMinX(int value) {
        PotholeParameters param = potAlg.getParams();
        param.imagewidthLimit = Range(value, param.imagewidthLimit.end);
        qDebug() << value;
        potAlg.setParams(param);
        Gpu_potAlg.setParams(param);
    }
    void setMaxX(int value) {
        PotholeParameters param = potAlg.getParams();
        param.imagewidthLimit = Range(param.imagewidthLimit.start, value);
        qDebug() << value;
        potAlg.setParams(param);
        Gpu_potAlg.setParams(param);
    }
    void setMinY(int value) {
        PotholeParameters param = potAlg.getParams();
        param.imageHeightLimit = Range(value, param.imageHeightLimit.end);
        qDebug() << value;
        potAlg.setParams(param);
        Gpu_potAlg.setParams(param);
    }
    void setMaxY(int value) {
        PotholeParameters param = potAlg.getParams();
        param.imageHeightLimit = Range(param.imageHeightLimit.start, value);
        qDebug() << value;
        potAlg.setParams(param);
        Gpu_potAlg.setParams(param);
    }



public:
    PotholeDetect(QWidget *parent = Q_NULLPTR);

private:
    void initializeGui();
    Ui::PotholeDetectClass ui;
    PotholeAlgorithm potAlg;
    GPUPotholeAlgorithm Gpu_potAlg;
    TestManager testManager;
    QImage copy;
    FrameCapture cap;
    VideoBuffer videoBuffer;

    bool isPaused = true;

    QAction* openVideoAct;
    QAction* openFileAct;

    QPushButton* playPauseBtn;
    QPushButton* previousFrameBtn;
    QPushButton* nextFrameBtn;

    QSpinBox* valueSpinBox;
    QSpinBox* rateoSpinBox;
    QSpinBox* kernelSizeSpinBox;
    QSpinBox* gradientThrSpinBox;
    QDoubleSpinBox* gradientValueSpinBox;

    QDoubleSpinBox* overlapSpinBox;
    QDoubleSpinBox* intersectSpinBox;
    QSpinBox* minXSpinBox;
    QSpinBox* maxXSpinBox;
    QSpinBox* minYSpinBox;
    QSpinBox* maxYSpinBox;

    //video label
    QLabel* view;
    QLabel* frameIndexLbl;
};
