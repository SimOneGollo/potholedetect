#pragma once

#include "Algorithm.h"
#include <opencv2/core.hpp>

#include "PotholeAlgorithm.h"
#include "GPUPotholeAlgorithm.h"

using namespace std;
using namespace cv;

class TestManager
{

public: 
	void initializeAlgorithm(PotholeAlgorithm* algorithm);
	void initializeGPUAlgorithm(GPUPotholeAlgorithm* GPUalgorithm);
	/// <summary>
	///		set version of algorithm to use.
	///		If return false you must initialize GPU version of algorithm, see initializeGPUAlgorithm function. 
	/// </summary>
	/// <param name="isEnable">flag to index if use standard or GPU version of algorithm</param>
	/// <returns>True if algorithm version is set correctly, oterwise return false.</returns>
	bool setGPUAlgorithm(bool isEnable);

	void loadTestResult(vector<PotholeResult> results) { this->results = results; }

	void getStatistics(float& precision, float& recall, float& accuracy) {
		if ((truePoisitive + falsePositive == 0) || (truePoisitive + falseNegative == 0)) {
			precision = 0;
			recall = 0;
			return;
		}
		precision = truePoisitive / (float)(truePoisitive + falsePositive);
		recall = truePoisitive / (float)(truePoisitive + falseNegative);
		accuracy = this->accuracy;
	}

	PotholeResult testImage(InputArray image, int frame, long long& time);

private:
	int truePoisitive = 0;
	int falsePositive = 0;
	int falseNegative = 0;

	float accuracy = 0;
	vector<PotholeResult> result;

	PotholeAlgorithm* algorithm = nullptr;
	GPUPotholeAlgorithm* GPUalgorithm = nullptr;

	vector<PotholeResult> results;

	bool isGPUEnable;
};

