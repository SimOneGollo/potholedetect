#include "VideoBuffer.h"


void VideoBuffer::buffImage(Mat image) {
	videoBuffer.push(image);
	conditionVariable.notify_one();
}

void VideoBuffer::run() {
	while (!QThread::currentThread()->isInterruptionRequested())
	{
		std::unique_lock<std::mutex> lck(buffMutex);
		while (!(videoBuffer.size() > 0 && nextReq)) {
			qDebug() << videoBuffer.size();
			conditionVariable.wait(lck);
		}
		nextReq = false;
		Mat image = videoBuffer.front().clone();
		videoBuffer.pop();
		emit newImage(image);
	}
}