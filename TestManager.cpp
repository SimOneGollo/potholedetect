#include "TestManager.h"

using namespace std::chrono;

PotholeResult TestManager::testImage(InputArray image, int frame, long long& time) {

	if (this->algorithm == nullptr) {
		return PotholeResult();
	}

	Mat H;
	auto t1 = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	Mat img;
	PotholeResult algRes;
	PotholeParameters params;
	if (this->isGPUEnable) {
		cv::imwrite("original.jpg", image);
		img = this->GPUalgorithm->wrapFrame(image, H);
		cv::imwrite("wrapped.jpg", img);
		algRes = this->GPUalgorithm->run(img.clone());
		params = GPUalgorithm->getParams();
	}
	else {
		Mat img = this->algorithm->wrapFrame(image, H);
		algRes = this->algorithm->run(img.clone());
		params = algorithm->getParams();
	}

	auto t2 = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	time = t2 - t1;

	// se le etichette non sono inizializzate, non valuta le metriche.
	if (this->results.empty()) {
		return algRes;
	}

	// se non ci sono le etichette per questo specifico frame e il risultato
	// dell'algoritmo � vuoto, non valuta le metriche.
	vector<PotholeResult> forframeSelection;
	copy_if(results.begin(), results.end(), back_inserter(forframeSelection),
		[frame](PotholeResult res) {return res.frame == frame; });

	if (forframeSelection.empty() && algRes.defects.empty()) {
		return algRes;
	}

	//inizio valutazione metriche.
	vector<Defect> tempDefect;
	// proietta le label nella vista dell'algoritmo e seleziona solo quelle
	// che rientrano nei limiti del campo di vista dell'algoritmo.
	for (Defect d : forframeSelection[0].defects) {
		if (d.contour.empty()) continue;
		vector<Point2f> contour;
		cv::perspectiveTransform(d.contour, contour, H);
		Defect projected(contour, d.id);

		if (projected.bbox.x > params.imagewidthLimit.start
			&& projected.bbox.x + projected.bbox.width < params.imagewidthLimit.end
			&& projected.bbox.y > params.mid_Cfinding // validi solo dopo la linea di finding
			&& projected.bbox.y + projected.bbox.height < params.imageHeightLimit.end) {

			tempDefect.push_back(Defect(contour, d.id));
		}
	}

	// se non ci sono label nei limiti dell'algoritmo, allora tutte le
	// etichette trovate sono considerate falsi positivi.
	PotholeResult forframe = PotholeResult::newResult(forframeSelection[0].frame, tempDefect, img.clone(), vector<float>());
	if (forframe.defects.empty()) {
		falsePositive += algRes.defects.size();
		return algRes;
	}
	vector<int> counterList(forframe.defects.size(), 0);

	/*
	 * Per ogni risultato trovato dall'algoritmo, lo si confronta con
	 * le etichette e se esiste una intersezione con IoU > 0.5 allora
	 * viene considerato un true positive, altrimenti se non viene 
	 * trovata nessuna inesezione viene salvato come falso positivo
	 */
	for (int defIndex = 0; defIndex < algRes.defects.size(); defIndex++) {
		int counter = 0;
		for (int i = 0; i < forframe.defects.size(); i++) {
			float IoU = checkIntersectionRateo(algRes.defects[defIndex], forframe.defects[i]);
			if (IoU > 0.5) {
				//esiste una intersezione tra reale e predetto
				truePoisitive++;
				counter++;
				algRes.accuracy.push_back(IoU);
				counterList[i] += 1;
			}
		}

		//quello predetto non interseca nessun reale
		if (counter == 0) {
			falsePositive++;
			algRes.accuracy.push_back(0);
		}
	}

	// per ogni reale che non � stato intersecato aggiungo un nuovo falso negativo
	for (int j : counterList) {
		if (j == 0) {
			falseNegative++;
		}
	}
	int counter = 0;
	for (float j : algRes.accuracy) {
		accuracy += j;
		if (j > 0) {
			counter++;
		}
	}
	accuracy /= counter;

	return algRes;
}


void TestManager::initializeAlgorithm(PotholeAlgorithm* algorithm) {
	this->algorithm = algorithm;
}

void TestManager::initializeGPUAlgorithm(GPUPotholeAlgorithm* GPUalgorithm) {
	this->GPUalgorithm = GPUalgorithm;
	this->setGPUAlgorithm(true);
}

bool TestManager::setGPUAlgorithm(bool isEnable) {
	if (this->GPUalgorithm != nullptr) {
		isGPUEnable = isEnable;
	}
	else {
		isGPUEnable = false;
		return false;
	}
	return true;
}