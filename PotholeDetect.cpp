#include "PotholeDetect.h"
#include "Conversion.h"
#include <fstream>

PotholeDetect::PotholeDetect(QWidget* parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	initializeGui();
	connect(&cap, SIGNAL(newImage(Mat)), &videoBuffer, SLOT(buffImage(Mat)));
	connect(&videoBuffer, SIGNAL(newImage(Mat)), this, SLOT(UpdateImage(Mat)));
	videoBuffer.start();
	potAlg = PotholeAlgorithm();
	testManager = TestManager();
	testManager.initializeAlgorithm(&potAlg);
	testManager.initializeGPUAlgorithm(&Gpu_potAlg);
}

void PotholeDetect::UpdateImage(Mat img) {
	long long time;
	PotholeResult res = testManager.testImage(img, cap.getFrameIndex(), time);
	Mat wImage = res.image;
	for (Defect d : res.defects) {
		cv::rectangle(wImage, d.bbox, Scalar(0, 0, 255));
	}
	float precision, recall, accuracy;
	testManager.getStatistics(precision, recall, accuracy);
	//Gpu_potAlg.getStatistics(precision, recall);

	qDebug() << "Precision: " << precision << " | Recall: " << recall << " | IoU(frame Average): " << accuracy << " |  Time: " << time;
	QImage image = Conversion::Mat2QImage(wImage);
	copy = image.copy();
	frameIndexLbl->setText(QString::number(cap.getFrameIndex()));
	view->setPixmap(QPixmap::fromImage(image.scaled(view->width(), view->height(), Qt::AspectRatioMode::KeepAspectRatio)));
	if (!isPaused) {
		videoBuffer.retrieveFrame();
	}
}

/// <summary>
///		slot per aprire il video.
/// </summary>
void PotholeDetect::openVideo() {
	QString fileName = QFileDialog::getOpenFileName(this,
		tr("Open Image"), "./", tr("Video Files (*.mp4)"));
	QStringList splitted = fileName.split('.');
	qDebug() << splitted.at(splitted.length() - 1);
	if (splitted.at(splitted.length() - 1) == "mp4")
	{
		cap.open(fileName.toStdString());
		playPauseBtn->setDisabled(false);
		previousFrameBtn->setDisabled(false);
		nextFrameBtn->setDisabled(false);
	}
}

void PotholeDetect::openFromFile() {
	QString fileName = QFileDialog::getOpenFileName(this,
		tr("Open Image"), "./", tr("Label file (*.label)"));
	ifstream labelFile(fileName.toStdString());
	string line;
	vector<PotholeResult> result;
	vector<vector<int>> data;
	bool isMetadata = true;
	int maxFrame = 0;
	while (getline(labelFile, line))
	{
		if (isMetadata) {
			cap.open(line);
			playPauseBtn->setDisabled(false);
			previousFrameBtn->setDisabled(false);
			nextFrameBtn->setDisabled(false);
			isMetadata = false;
		}
		else {
			vector<int> temp;
			QStringList splitted = QString::fromStdString(line).split(',');
			for (QString s : splitted) {
				temp.push_back(s.toInt());
			}
			data.push_back(temp);
			maxFrame = temp[1] > maxFrame ? temp[1] : maxFrame;
		}
	}
	/*
	 * Per ogni frame prendo i dati e ricostruisco bbox
	 */
	for (int i = 0; i <= maxFrame; i++) {
		vector<vector<int>> temp;
		copy_if(data.begin(), data.end(), back_inserter(temp), [i](vector<int> defect) {return defect[0] == i; });
		vector<Defect> dList;
		for (vector<int> label : temp) {

			int p1x, p2x, p1y, p2y;
			p1x = label[2] * 0.5;
			p2x = label[4] * 0.5;
			p1y = label[3] * 0.5;
			p2y = label[5] * 0.5;

			if (p2x < p1x) {
				int temp;
				temp = p1x;
				p1x = p2x;
				p2x = temp;
				temp = p1y;
				p1y = p2y;
				p2y = temp;
			}

			dList.push_back(Defect(Rect2d(Point2d(p1x, p1y), Point2d(p2x, p2y)), label[0]));
		}
		result.push_back(PotholeResult::newResult(i, dList, Mat(), vector<float>()));
	}
	testManager.loadTestResult(result);
}

/// <summary>
///		slot per far partire e mettere in pausa il video
/// </summary>
void PotholeDetect::playPause() {
	if (isPaused) {
		qDebug() << "start command";
		cap.start();
		playPauseBtn->setText("||");
		nextFrameBtn->setDisabled(true);
		previousFrameBtn->setDisabled(true);
		isPaused = !isPaused;
		videoBuffer.retrieveFrame();
	}
	else {
		qDebug() << "pause command";
		cap.requestInterruption();
		playPauseBtn->setText(">");
		nextFrameBtn->setDisabled(false);
		previousFrameBtn->setDisabled(false);
		isPaused = !isPaused;
	}
}

/// <summary>
///		slot per ottenere il frame successivo a quello attuale;
/// </summary>
void PotholeDetect::nextFrame() {
	//cap.grab();
	// to fast debug
	if (!cap.grab()) {
		cap.open("C:\\Users\\Simone\\Desktop\\Universita\\Tirocinio\\PotholePyTest\\test_video\\N_OK\\20210221173800_29.mp4");
		cap.grab();
	}
	videoBuffer.retrieveFrame();
}

/// <summary>
///		slot per ottenere il frame successivo a quello attuale;
/// </summary>
void PotholeDetect::previousFrame() {
	cap.goToFrame(cap.getFrameIndex() - 1);
	videoBuffer.retrieveFrame();
}


/// <summary>
///		Funzione di inizializzazione della gui e collegamento dei signal e slot 
/// </summary>
void PotholeDetect::initializeGui() {

	view = ui.view;
	//file men�
	openVideoAct = ui.openVideoAct;
	openFileAct = ui.openFileAct;

	connect(openVideoAct, &QAction::triggered, this, &PotholeDetect::openVideo);
	connect(openFileAct, &QAction::triggered, this, &PotholeDetect::openFromFile);

	playPauseBtn = ui.playPause;
	previousFrameBtn = ui.previousFrame;
	nextFrameBtn = ui.nextFrame;
	frameIndexLbl = ui.frameIndexLbl;

	connect(playPauseBtn, SIGNAL(clicked()), this, SLOT(playPause()));
	connect(previousFrameBtn, SIGNAL(clicked()), this, SLOT(previousFrame()));
	connect(nextFrameBtn, SIGNAL(clicked()), this, SLOT(nextFrame()));

	//playPauseBtn->setDisabled(true);
	//previousFrameBtn->setDisabled(true);
	//nextFrameBtn->setDisabled(true);

	valueSpinBox = ui.valueSpinBox;
	rateoSpinBox = ui.rateoSpinBox;
	kernelSizeSpinBox = ui.kernelSizeSpinBox;
	gradientThrSpinBox = ui.gradientThrSpinBox;
	gradientValueSpinBox = ui.gradientValueSpinBox;

	overlapSpinBox = ui.overlapSpinBox;
	intersectSpinBox = ui.intersectSpinBox;
	minXSpinBox = ui.minXSpinBox;
	maxXSpinBox = ui.maxXSpinBox;
	minYSpinBox = ui.minYSpinBox;
	maxYSpinBox = ui.maxYSpinBox;

	connect(valueSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setCannyValue(int)));
	connect(rateoSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setCannyRateo(int)));
	connect(kernelSizeSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setCannyKernelSize(int)));
	connect(gradientThrSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setGradientMinThr(int)));
	connect(gradientValueSpinBox, SIGNAL(valueChanged(double)), this, SLOT(setGradientValue(double)));

	connect(overlapSpinBox, SIGNAL(valueChanged(double)), this, SLOT(setOverlapRateo(double)));
	connect(intersectSpinBox, SIGNAL(valueChanged(double)), this, SLOT(setIntersectRateo(double)));
	connect(minXSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setMinX(int)));
	connect(maxXSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setMaxX(int)));
	connect(minYSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setMinY(int)));
	connect(maxYSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setMaxY(int)));
}