#include "PotholeAlgorithm.h"

using namespace std::chrono;

PotholeResult PotholeAlgorithm::run(InputArray image) {

	Mat img = image.getMat();
	Mat working_image = img(params.imageHeightLimit, params.imagewidthLimit);

	vector<Scalar> color{ params.lowerWhite, params.higherWhite, params.lowerYellow, params.highterYellow };
	Mat lineMask = getRoadLineMask(img, color);

	Mat start_image_gray;
	cv::cvtColor(working_image, start_image_gray, cv::COLOR_BGR2GRAY);

	Mat edge;
	vector<vector<Point2f>> contours = extractContourCanny(start_image_gray(Range(params.start_Cfinding, params.stop_Cfinding), Range(0, working_image.cols)), edge);

	trackPreviousDefect(working_image);

	vector<Defect> defects;
	for (vector<Point2f> c : contours) {
		vector<Point2f> realContours;
		for (Point p : c) {
			realContours.push_back(p + Point(params.imagewidthLimit.start, params.imageHeightLimit.start + params.start_Cfinding));
		}
		Defect newD(realContours, getNewId());
		if (checkElongationAndArea(newD, 0.1, params.minAreaPx)) {
			defects.push_back(newD);
		}
	}

	vector<Defect> newDefects;
	if (!defects.empty()) {
		newDefects = trackNewDefect(img, lineMask, defects, prevDefect, params.mid_Cfinding);
		for (Defect d : newDefects) {
			prevDefect.push_back(d);
		}
	}

	return PotholeResult::newResult(0, prevDefect, img, vector<float>());
}

vector<Defect> PotholeAlgorithm::trackNewDefect(InputArray image, InputArray mask, vector<Defect> newDefects, vector<Defect> oldDfects, int startTrack) {
	Mat test = image.getMat().clone();
	vector<Defect> resultDefect;

	vector<Defect> joinedDefects = joinOverlapped(newDefects, params.overlappedRateo);

	for (Defect defect : joinedDefects) {
		bool isNew = true;
		if (filterDefect(mask, defect)) {
			continue;
		}

		//if almost one defect intersect an old defect, the new one is skipped.
		for (Defect old : oldDfects) {
			if (checkIntersectionRateo(defect, old, params.intersectionRateo)) {
				isNew = false;
				break;
			}
		}

		if (isNew) {
			//maybe need conversion to grayscale
			Mat gradient = defectGradient(image);
			if (gradientAnalisy(gradient, defect.bbox, params.gradientVal, params.gradientMinThr)) {
				defect.initTracker(image);
				resultDefect.push_back(defect);
			}
		}
	}

	return resultDefect;
}

vector<Defect> PotholeAlgorithm::joinOverlapped(vector<Defect> defects, float overPercentage) {

	bool flaggetto = false;

	for (int i = 0; i < defects.size(); i++) {
		flaggetto = false;
		for (int j = i + 1; j < defects.size(); j++) {
			if (checkIntersectionRateo(defects[i], defects[j], params.overlappedRateo)) {

				vector<Point2f> newContour = defects[i].contour;
				newContour.insert(newContour.end(), defects[j].contour.begin(), defects[j].contour.end());

				defects.push_back(Defect(newContour, getNewId()));

				defects.erase(defects.begin() + j);
				defects.erase(defects.begin() + i);
				flaggetto = true;
				break;
			}
		}
		if (flaggetto) {
			i--;
		}
	}

	return defects;
}

vector<vector<Point2f>> PotholeAlgorithm::extractContourCanny(InputArray image, OutputArray output) {
	Mat img = image.getMat();
	Mat edge;
	cv::Canny(image, edge, params.cannyVal, params.cannyVal * params.ratio, params.kernelSize);
	cv::morphologyEx(edge, output, cv::MORPH_CLOSE, kernel33);
	vector<vector<Point>> contours;
	cv::findContours(output, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
	vector<vector<Point2f>> contoursf;
	for (vector<Point> cont : contours) {
		vector<Point2f> contf;
		std::transform(cont.begin(), cont.end(), back_inserter(contf),
			[](Point t) { return (Point2f)t; });
		contoursf.push_back(contf);
	}

	return contoursf;
}

Mat PotholeAlgorithm::wrapFrame(InputArray image, Mat& H) {
	int h0 = image.getMat().rows;
	int w0 = image.getMat().cols;
	int nc0 = image.getMat().channels();
	Rect ROI = Rect(Point(0, 0), Point(w0, h0 - 80));
	Mat frame = image.getMat()(ROI);
	Mat resized;
	cv::resize(frame, resized, Size(), sf, sf, cv::INTER_AREA);

	int w2 = (w0 * sf) / 2;
	int h2 = (h0 * sf) / 2;
	int r0 = 28 * 1.5;
	int r1 = 150 * 1.5;
	int r2 = r1 + 70;
	int h3 = std::min(resized.rows, (int)(h2 * 1.5));


	Mat px = (cv::Mat_<float>(4, 2) << w2 - r0, h2,
		w2 + r0, h2,
		w2 + r1, h3,
		w2 - r1, h3);

	Mat pModel = (cv::Mat_<float>(4, 2) << 0, -h3,
		r1, -h3,
		r1, h3,
		0, h3);

	H = cv::getPerspectiveTransform(px, pModel);
	Mat frame_wrap;
	cv::warpPerspective(resized, frame_wrap, H, Size(r1, h3));

	return frame_wrap;
}

bool PotholeAlgorithm::filterDefect(InputArray mask, Defect defect) {

	if (!checkElongationAndArea(defect, 0.1, params.minAreaPx)) {
		return true;
	}
	
	int counter = 0;
	Mat matMask = mask.getMat();

	for (Point p : defect.contour) {
		if (matMask.at<unsigned char>(p.y, p.x) == 255) counter++;
	}

	return counter / defect.contour.size() > params.filterSensitivity;
}

void PotholeAlgorithm::trackPreviousDefect(Mat image) {
	vector<Defect> result;

	for (int i = 0; i < prevDefect.size(); i++) {
		if (prevDefect[i].trackUpdate(image)) {
			result.push_back(prevDefect[i]);
		}
	}

	prevDefect = result;
}

int PotholeAlgorithm::getNewId() {
	return lastId++;
}