#include "GPUPotholeAlgorithm.h"

PotholeResult GPUPotholeAlgorithm::run(InputArray image)
{

	Mat img = image.getMat();
	cv::cuda::Stream stream;
	vector<Scalar> color{params.lowerWhite, params.higherWhite, params.lowerYellow, params.highterYellow};

	Mat working_image = img(params.imageHeightLimit, params.imagewidthLimit);
	cv::cuda::GpuMat Gpu_working_image, srcImage;
	srcImage.upload(img, stream);
	Gpu_working_image.upload(working_image, stream);

	cv::cuda::GpuMat lineMask = getRoadLineMask_GPU(srcImage, color, stream);

	cv::cuda::GpuMat start_image_gray;
	cv::cuda::cvtColor(Gpu_working_image, start_image_gray, cv::COLOR_BGR2GRAY, 0, stream);

	cv::cuda::GpuMat edge;
	vector<vector<Point2f>> contours = extractContourCanny(start_image_gray(Range(params.start_Cfinding, params.stop_Cfinding), Range(0, working_image.cols)), edge, stream);

	trackPreviousDefect(working_image);

	vector<Defect> defects((size_t)contours.size());
	int nContours = contours.size();

	for (int i = 0; i < nContours; i++)
	{
		vector<Point2f> realContours;
		for (Point p : contours[i])
		{
			realContours.push_back(p + Point(params.imagewidthLimit.start, params.imageHeightLimit.start + params.start_Cfinding));
		}
		Defect newD(realContours, getNewId(), 7);
		if (checkElongationAndArea(newD, params.elongationRateo, params.minAreaPx))
		{
			defects[i] = newD;
		}
	}

	vector<Defect> newDefects;

	if (!defects.empty())
	{
		newDefects = trackNewDefect(srcImage, lineMask, defects, prevDefect, params.mid_Cfinding, stream);
		for (Defect d : newDefects)
		{
			prevDefect.push_back(d);
		}
	}

	return PotholeResult::newResult(0, prevDefect, img, vector<float>());
}

vector<Defect> GPUPotholeAlgorithm::trackNewDefect(InputArray image, InputArray mask, vector<Defect> newDefects, vector<Defect> oldDfects, int startTrack, cv::cuda::Stream &stream)
{

	vector<Defect> resultDefect;
	cv::cuda::GpuMat gradient = GpuDefectGradient(image, stream);
	Mat CpuGrad, im;
	gradient.download(CpuGrad, stream);
	image.getGpuMat().download(im, stream);

	vector<Defect> joinedDefects = joinOverlapped(newDefects, params.overlappedRateo);
	stream.waitForCompletion();

	for (Defect defect : joinedDefects)
	{
		bool isNew = true;
		if (filterDefect(mask, defect))
		{
			continue;
		}

		//if almost one defect intersect an old defect, the new one is skipped.
		for (Defect old : oldDfects)
		{
			if (checkIntersectionRateo(defect, old, params.intersectionRateo))
			{
				isNew = false;
				break;
			}
		}

		if (isNew)
		{
			//maybe need conversion to grayscale
			if (gradientAnalisy(CpuGrad, defect.bbox, params.gradientVal, params.gradientMinThr))
			{
				defect.initTracker(im);
				resultDefect.push_back(defect);
			}
		}
	}

	return resultDefect;
}

vector<Defect> GPUPotholeAlgorithm::joinOverlapped(vector<Defect> defects, float overPercentage)
{

	bool flaggetto = false;

	for (int i = 0; i < defects.size(); i++)
	{
		flaggetto = false;
		for (int j = i + 1; j < defects.size(); j++)
		{
			if (checkIntersectionRateo(defects[i], defects[j], params.overlappedRateo))
			{

				vector<Point2f> newContour = defects[i].contour;
				newContour.insert(newContour.end(), defects[j].contour.begin(), defects[j].contour.end());

				defects.push_back(Defect(newContour, getNewId(), params.defectPadding));

				defects.erase(defects.begin() + j);
				defects.erase(defects.begin() + i);
				flaggetto = true;
				break;
			}
		}
		if (flaggetto)
		{
			i--;
		}
	}

	return defects;
}

vector<vector<Point2f>> GPUPotholeAlgorithm::extractContourCanny(InputArray image, OutputArray output, cv::cuda::Stream &stream)
{
	cv::cuda::GpuMat img = image.getGpuMat();
	cv::cuda::GpuMat Gpu_edge;
	cv::Ptr<cv::cuda::CannyEdgeDetector> canny_edg = cv::cuda::createCannyEdgeDetector(params.cannyVal, params.cannyVal * params.ratio, params.kernelSize);
	canny_edg->detect(img, Gpu_edge, stream);

	Ptr<cv::cuda::Filter> f = cv::cuda::createMorphologyFilter(MORPH_CLOSE, Gpu_edge.type(), kernel33);
	f->apply(Gpu_edge, Gpu_edge, stream);
	stream.waitForCompletion();
	Mat edge;
	Gpu_edge.download(edge);
	cv::imshow("test", edge);
	cv::waitKey(1);
	Gpu_edge.copyTo(output);
	vector<vector<Point>> contours;
	cv::findContours(edge, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
	vector<vector<Point2f>> contoursf;

	for (vector<Point> cont : contours)
	{
		vector<Point2f> contf;
		std::transform(cont.begin(), cont.end(), back_inserter(contf),
									 [](Point t)
									 { return (Point2f)t; });
		contoursf.push_back(contf);
	}
	return contoursf;
}

bool GPUPotholeAlgorithm::filterDefect(InputArray mask, Defect defect)
{

	if (defect.contour.size() == 0)
	{
		return true;
	}

	int counter = 0;
	Mat matMask;
	mask.getGpuMat().download(matMask);

	vector<int> resList;

	parallel_for_(Range(0, defect.contour.size()), [&](const Range &range)
								{
									int begin = range.start;
									int end = range.end;

									int counter = 0;
									for (int i = begin; i < end; i++)
									{
										Point p = defect.contour[i];
										if (matMask.at<unsigned char>(p.y, p.x) == 255)
											counter++;
									}
									mtx.lock();
									resList.push_back(counter);
									mtx.unlock();
								});

	return counter / defect.contour.size() > params.filterSensitivity;
}

void GPUPotholeAlgorithm::trackPreviousDefect(Mat image)
{
	int prevSize = prevDefect.size();
	vector<Defect> result;

	for (int i = 0; i < prevSize; i++)
	{
		if (prevDefect[i].trackUpdate(image))
		{
			result.push_back(prevDefect[i]);
		}
	}

	prevDefect = result;
}

Mat GPUPotholeAlgorithm::wrapFrame(InputArray image, Mat &H)
{
	int h0 = image.getMat().rows;
	int w0 = image.getMat().cols;
	int nc0 = image.channels();
	Rect ROI = Rect(Point(0, 0), Point(w0, h0 - 80));
	Mat frame = image.getMat()(ROI);
	Mat resized;
	cv::resize(frame, resized, Size(), sf, sf, cv::INTER_AREA);
	int w2 = (w0 * sf) / 2;
	int h2 = (h0 * sf) / 2;
	int r0 = 28 * 1.5;
	int r1 = 150 * 1.5;
	int r2 = r1 + 70;
	int h3 = std::min(resized.rows, (int)(h2 * 1.5));

	Mat px = (cv::Mat_<float>(4, 2) << w2 - r0, h2,
						w2 + r0, h2,
						w2 + r1, h3,
						w2 - r1, h3);

	Mat pModel = (cv::Mat_<float>(4, 2) << 0, -h3,
								r1, -h3,
								r1, h3,
								0, h3);

	H = cv::getPerspectiveTransform(px, pModel);
	Mat frame_wrap;
	cv::warpPerspective(resized, frame_wrap, H, Size(r1, h3));

	return frame_wrap;
}

int GPUPotholeAlgorithm::getNewId()
{
	return lastId++;
}