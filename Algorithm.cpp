#include "Algorithm.h"


#pragma region struct static function
PotholeParameters PotholeParameters::getDefaultParams() {
	PotholeParameters p;

	p.cannyVal = 15;
	p.ratio = 5;
	p.kernelSize = 3;

	p.gradientMinThr = 25;
	p.gradientVal = 2.5f;

	p.intersectionRateo = 0.6;
	p.overlappedRateo = 0.4;
	p.minAreaPx = 50;

	p.filterSensitivity = 0.3;

	p.lowerWhite = Scalar(0, 0, 150);
	p.higherWhite = Scalar(180, 20, 255);
	p.lowerYellow = Scalar(18, 94, 140);
	p.highterYellow = Scalar(48, 255, 255);

	p.start_Cfinding = 50;
	p.mid_Cfinding = 125;
	p.stop_Cfinding = 200;

	p.imageHeightLimit = Range(0, 370);
	p.imagewidthLimit = Range(39, 185);

	p.defectPadding = 3;

	p.elongationRateo = 0.15;

	return p;
}

PotholeResult PotholeResult::newResult(int frame, vector<Defect> defects, Mat image, vector<float> accuracy) {
	PotholeResult result = PotholeResult();
	result.frame = frame;
	result.defects = defects;
	result.image = image;
	result.accuracy = accuracy;
	return result;
}

#pragma endregion