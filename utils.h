#pragma once
#include "Defect.h"
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>;

#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudafeatures2d.hpp>
#include <opencv2/cudaimgproc.hpp>

using namespace cv;
using namespace std;


static float checkIntersection(Defect a, Defect b, int& code) {
	Rect intersectBox = a.bbox & b.bbox;
	if (intersectBox.area() > 0) {
		if (intersectBox.area() == a.bbox.area() ||
			intersectBox.area() == b.bbox.area()) {
			code = INTERSECT_FULL;
		}
		else {
			code = INTERSECT_PARTIAL;
		}

		return intersectBox.area();	
	}

	code = INTERSECT_NONE;
	return 0;

	//vector<Point> out;
	//code = cv::rotatedRectangleIntersection(a.minbbox, b.minbbox, out);
	//if (code == INTERSECT_NONE) {
	//	return 0;
	//}
	//return cv::minAreaRect(out).size.area();
}

/// <summary>
///		
/// </summary>
/// <param name="a">First defect</param>
/// <param name="b">Second defect</param>
/// <param name="percentage">percentage of area to consider a defect intersected by another one</param>
/// <returns>true if intersection area percentage is more than percentage parameter</returns>
static bool checkIntersectionRateo(Defect a, Defect b, float percentage) {
	int code;
	float area = checkIntersection(a, b, code);

	switch (code)
	{
	case INTERSECT_FULL:
		return true;
		break;
	case INTERSECT_NONE:
		return false;
	case INTERSECT_PARTIAL:
		return area / a.bbox.area() > percentage || area / b.bbox.area() > percentage;
		break;
	}
	return false;
}

/// <summary>
///		
/// </summary>
/// <param name="a">First defect</param>
/// <param name="b">Second defect</param>
/// <returns>return accuracy of intersection (Intersection over Union metrics)</returns>
static float checkIntersectionRateo(Defect a, Defect b) {
	int code;
	float area = checkIntersection(a, b, code);

	switch (code)
	{
	case INTERSECT_NONE:
		return 0;
	case INTERSECT_PARTIAL: 
	case INTERSECT_FULL:
		return area / (a.bbox.area() + b.bbox.area() - area);
		break;
	}
}


static Mat defectGradient(InputArray image) {
	Mat gray, dx, dy;
	cv::cvtColor(image, gray, cv::COLOR_BGR2GRAY);
	Sobel(gray, dx, CV_16S, 1, 0);
	Sobel(gray, dy, CV_16S, 0, 1);

	Mat abs_dx, abs_dy, gradient;
	cv::convertScaleAbs(dx, abs_dx);
	cv::convertScaleAbs(dy, abs_dy);
	cv::addWeighted(abs_dx, 0.5, abs_dy, 0.5, 0, gradient);

	return gradient;
}

static cv::cuda::GpuMat GpuDefectGradient(InputArray image, cv::cuda::Stream& stream) {
	cv::cuda::GpuMat gray, dx, dy;
	cv::cuda::cvtColor(image, gray, cv::COLOR_BGR2GRAY, 0, stream);

	Ptr< cv::cuda::Filter >sobelF1 = cv::cuda::createSobelFilter(gray.type(), CV_16S, 1, 0);
	Ptr< cv::cuda::Filter >sobelF2 = cv::cuda::createSobelFilter(gray.type(), CV_16S, 0, 1);
	sobelF1->apply(gray, dx, stream);
	sobelF2->apply(gray, dy, stream);

	cv::cuda::GpuMat gradient, abs_gradient;
	Mat grad, abs_grad;

	cv::cuda::addWeighted(dx, 0.5, dy, 0.5, 0, gradient, -1 , stream);
	gradient.download(grad,stream);
	stream.waitForCompletion();
	cv::convertScaleAbs(grad, abs_grad);
	abs_gradient.upload(abs_grad);

	return abs_gradient;
}

static void plotHist(Mat hist, string name = "hist") {
	int hist_w = 512, hist_h = 400;
	int bin_w = cvRound((double)hist_w / 256);
	Mat cHist = hist.clone();
	Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));
	Mat outHist;
	cv::normalize(cHist, outHist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
	int t = outHist.type();
	for (int i = 1; i < 256; i++)
	{
		line(histImage, Point(bin_w * (i - 1), hist_h - cvRound(outHist.at<float>(i - 1))),
			Point(bin_w * (i), hist_h - cvRound(outHist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
	}
	cv::imshow(name, histImage);
}


static Rect getDefectBoundary(Size gradientImageSize, Rect defectBox) {
	int x = defectBox.x < 0 ? 0 : defectBox.x;
	int y = defectBox.y < 0 ? 0 : defectBox.y;

	int x2 = x + defectBox.width;
	int y2 = y + defectBox.height;

	int sx = x2 > gradientImageSize.width - 1 ? gradientImageSize.width - 1 - x : defectBox.width;
	int sy = y2 > gradientImageSize.height - 1 ? gradientImageSize.height - 1 - y : defectBox.height;

	return Rect(x, y, sx, sy);
}

static bool gradientAnalisy(InputArray gradientImage, Rect defectBox, float stdThr, int lowThr = 0) {

	Rect defectLimit = getDefectBoundary(gradientImage.getMat().size(), defectBox);

	Mat test = gradientImage.getMat();

	Mat ROI = gradientImage.getMat()(defectLimit);
	Mat hist;
	int histSize = 256;
	float range[] = { 0, 256 };
	const float* histRange = { range };

	cv::calcHist(&ROI, 1, 0, Mat(), hist, 1, &histSize, &histRange, true, false);
	Mat mask = Mat::zeros(hist.size(), CV_8U);

	for (int i = lowThr; i < 256; i++) {
		mask.at<unsigned char>(i, 0) = 255;
	}
	//plotHist(hist, "hitstMask");
	Mat mean, std;
	cv::meanStdDev(hist, mean, std, mask);

	int t = std.type();

	//TODO: Test std data type and size
	double stdData = std.at<double>(0, 0);
	return stdData >= stdThr;
}

//static bool gradientAnalisy_GPU(InputArray gradientImage, Rect defectBox, float stdThr, int lowThr = 0) {
//
//	Rect defectLimit = getDefectBoundary(gradientImage.getGpuMat().size(), defectBox);
//
//	cv::cuda::GpuMat ROI = gradientImage.getGpuMat()(defectLimit);
//	cv::cuda::GpuMat hist(1, 256, CV_32SC1);
//
//	int i = ROI.type();
//
//	cv::cuda::calcHist(ROI, hist);
//
//	Mat mask = Mat::zeros(hist.size(), CV_8U);
//	for (int i = lowThr; i < 256; i++) {
//		mask.at<unsigned char>(0, i) = 255;
//	}
//
//	Mat mean, std, CPUHist;
//	hist.download(CPUHist);
//	cv::meanStdDev(hist, mean, std, mask);
//
//	return std.at<double>(0, 0) >= stdThr;
//}

/// <summary>
///		return true if elongation and area properties are convalidated.
/// </summary>
/// <param name="defect"></param>
/// <param name="minArea"></param>
/// <returns></returns>
static bool checkElongationAndArea(Defect defect, float elongationRateo = 0.1, int minArea = 100) {
	if (defect.minbbox.size.width == 0 || defect.minbbox.size.height == 0)
		return false;

	float r = defect.minbbox.size.width < defect.minbbox.size.height ?
		(float)defect.minbbox.size.width / defect.minbbox.size.height :
		(float)defect.minbbox.size.height / defect.minbbox.size.width;

	if (r < elongationRateo || defect.minbbox.size.area() < minArea) {
		return false;
	}

	return true;
}

static Mat getRoadLineMask(InputArray image, vector<Scalar> whiteYellowScalar, int kSize = 3) {
	Mat img = image.getMat();

	Mat hsv;
	cv::cvtColor(image, hsv, cv::COLOR_BGR2HSV);
	Mat maskYellow, maskWhite;
	cv::inRange(hsv, whiteYellowScalar[0], whiteYellowScalar[1], maskYellow);
	cv::inRange(hsv, whiteYellowScalar[2], whiteYellowScalar[3], maskWhite);
	Mat mask;
	cv::bitwise_or(maskWhite, maskYellow, mask);
	cv::dilate(mask, mask, Mat::ones(Size(kSize, kSize), CV_8U));

	return mask;
}

static cv::cuda::GpuMat getRoadLineMask_GPU(InputArray image, vector<Scalar> whiteYellowScalar, cv::cuda::Stream& stream, Mat kernel = Mat::ones(Size(3, 3), CV_8U)) {
	cv::cuda::GpuMat img = image.getGpuMat();
	cv::cuda::GpuMat out;
	Mat hsv;
	cv::cuda::cvtColor(img, out, cv::COLOR_BGR2HSV, 0, stream);

	//trasportabile in cuda con una versione aggiornata della libreria
	cv::cuda::GpuMat maskYellow, maskWhite;

	cv::cuda::inRange(out, whiteYellowScalar[0], whiteYellowScalar[1], maskYellow, stream);
	cv::cuda::inRange(out, whiteYellowScalar[2], whiteYellowScalar[3], maskWhite, stream);

	cv::cuda::GpuMat mask;
	cv::cuda::bitwise_or(maskYellow, maskWhite, mask, noArray(), stream);
	Ptr<cv::cuda::Filter> f = cv::cuda::createMorphologyFilter(MORPH_DILATE, mask.type(), kernel);
	f->apply(mask, mask, stream);

	return mask;
}