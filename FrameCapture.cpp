#include "FrameCapture.h"

void FrameCapture::run() {
	Mat image = Mat();
	int waitTime = 1000 / cap.get(cv::CAP_PROP_FPS);
	while (!QThread::currentThread()->isInterruptionRequested() && internalGrab(image))
	{
		emit newImage(image);
		msleep(waitTime);
	}
}

void FrameCapture::open(std::string filename) {
	cap.release();
	cap = VideoCapture(filename);
	this->videoFile = filename;
	qDebug() << "Video opened: " << cap.isOpened();
	width = cap.get(CAP_PROP_FRAME_WIDTH);
	height = cap.get(CAP_PROP_FRAME_HEIGHT);
}

void FrameCapture::skipFrame(int n) {
	//si muove al frame n-1 esimo
	int reachIndex = getFrameIndex() + n;
	goToFrame(reachIndex);
	qDebug() << cap.get(CAP_PROP_POS_FRAMES);
}

void FrameCapture::goToFrame(int n) {
	//muove al frame n-1 esimo
	cap.set(CAP_PROP_POS_FRAMES, n - 1);
	//prende il frame n-essimo
	Mat image = Mat();
	internalGrab(image);
	emit newImage(image);
}


bool FrameCapture::grab() {
	Mat image = Mat();
	if (internalGrab(image)) {
		emit newImage(image);
		return true;
	}
	return false;
}

bool FrameCapture::internalGrab(Mat& image) {
	if (cap.isOpened() && cap.grab()) {
		cap.retrieve(image);
		return true;
	}
	return false;
}
