#pragma once
#include "Defect.h"
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>
#include "utils.h"

#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudafeatures2d.hpp>
#include <opencv2/cudaimgproc.hpp>
using namespace std;
using namespace cv;

struct PotholeResult {
	vector<Defect> defects;
	int frame;
	Mat image;
	vector<float> accuracy;

	static PotholeResult newResult(int frame, vector<Defect> defects, Mat image, vector<float> accuracy);

	bool operator==(PotholeResult a) const{
		return this->frame == a.frame 
			&& this->defects.size() == a.defects.size();
	}
};

struct PotholeParameters
{
	int cannyVal;
	int ratio;
	int kernelSize;

	int binaryVal;
	int C;

	int gradientMinThr;
	float gradientVal;

	float intersectionRateo;
	float overlappedRateo;
	int minAreaPx;

	float filterSensitivity;

	Scalar lowerWhite;
	Scalar higherWhite;
	Scalar lowerYellow;
	Scalar	highterYellow;

	int start_Cfinding;
	int mid_Cfinding;
	int stop_Cfinding;
	Range imageHeightLimit;
	Range imagewidthLimit;

	int defectPadding;

	int elongationRateo;

	static PotholeParameters getDefaultParams();
};

namespace Pothole {

	class Algorithm
	{
	public:
		virtual PotholeResult run(InputArray image) = 0;
		virtual Mat wrapFrame(InputArray image, Mat& H) = 0;
		virtual PotholeParameters getParams() = 0;
	};

}

