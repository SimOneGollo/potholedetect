#include "Algorithm.h"
#include "Defect.h"
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>
#include "utils.h"

#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudafeatures2d.hpp>
#include <opencv2/cudaimgproc.hpp>

using namespace cv;
#pragma once
class GPUPotholeAlgorithm:Pothole::Algorithm
{
public:
	GPUPotholeAlgorithm() {
		gpuKernel.upload(Mat::ones(Size(3, 3), CV_8U));
	}

	PotholeResult run(InputArray image) override;
	Mat wrapFrame(InputArray image, Mat& H) override;
	PotholeParameters getParams() override {
		return this->params;
	}
	void setParams(PotholeParameters params) { this->params = params; }
	void loadTestResult(vector<PotholeResult> results) { this->results = results; }

private:
	vector<vector<Point2f>> extractContourCanny(InputArray image, OutputArray output, cv::cuda::Stream& stream);
	void trackPreviousDefect(Mat image);
	vector<Defect> trackNewDefect(InputArray image, InputArray mask, vector<Defect> newDefects, vector<Defect> oldDfects, int startTrack, cv::cuda::Stream& stream);

	vector<Defect> joinOverlapped(vector<Defect> defects, float overPercentage);
	bool filterDefect(InputArray mask, Defect defect);

	int lastId = 0;
	float sf = 0.5;

	vector<Defect> prevDefect;
	vector<Defect> outDefect;
	Mat kernel33 = Mat::ones(Size(3, 3), CV_8U);
	cv::cuda::GpuMat gpuKernel;
	PotholeParameters params = PotholeParameters::getDefaultParams();
	cv::Mutex mtx;
	cv::Mutex prevMtx;

	int getNewId();
	vector<PotholeResult> results;
	int TP = 0, FP = 0, FN = 0;
};

