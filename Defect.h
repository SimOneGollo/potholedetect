#pragma once

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/tracking.hpp>

using namespace cv;
using namespace std;

class Defect
{

public:
	Defect();
	Defect(vector<Point2f> contours, int id, int padding = 0);
	Defect(Rect bbox, int id, int padding = 0);

	void initTracker(InputArray image);
	bool trackUpdate(InputArray image);

	bool operator==(Defect b);

	Ptr<TrackerKCF> tracker = nullptr;

	vector<Point2f> contour;
	Rect bbox;
	RotatedRect minbbox;

	int id;
	int trackLife = 0;

	float elongation = 0;

private:
	Rect addPadding(Rect box, int padding);
};

