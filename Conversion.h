#pragma once

#include <qdebug.h>
#include <QImage>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>


using namespace cv;

class Conversion
{

public:
	static QImage Mat2QImage(Mat const& src, int matType)
	{
		QImage::Format format = formatConversion(matType);
		QImage dest((const uchar*)src.data, src.cols, src.rows, src.step, format);
		dest.bits(); // enforce deep copy, see documentation 
		return dest;
	}

	static QImage Mat2QImage(Mat const& src)
	{
		return Conversion::Mat2QImage(src, src.type());
	}

	static Mat QImage2Mat(QImage& src, QImage::Format format)
	{
		int cvFormat = formatConversion(format);
		Mat im = Mat(src.height(), src.width(), cvFormat, src.bits(), src.bytesPerLine()).clone();
		if (format == QImage::Format::Format_RGB32) {
			cvtColor(im, im, COLOR_BGRA2BGR);
		}
		return im;
	}

	static Mat QImage2Mat(QImage& src)
	{
		return Conversion::QImage2Mat(src, src.format());
	}

private:
	Conversion() {}

	static QImage::Format formatConversion(int matType)
	{
		QImage::Format format;
		switch (matType)
		{
		case CV_8U:
			format = QImage::Format::Format_Grayscale8;
			break;
		case CV_8UC3:
			format = QImage::Format::Format_BGR888;
			break;
		case CV_8UC4:
			format = QImage::Format::Format_ARGB32;
			break;
		default:
			format = QImage::Format::Format_BGR888;
			break;
		}
		return format;
	}

	static int formatConversion(QImage::Format format)
	{
		int cvFormat = 0;
		switch (format)
		{
		case  QImage::Format::Format_Grayscale8:
			cvFormat = CV_8U;
			break;
		case  QImage::Format::Format_BGR888:
			cvFormat = CV_8UC3;
			break;
		case QImage::Format::Format_ARGB32:
		case QImage::Format::Format_RGB32:
			cvFormat = CV_8UC4;
			break;
		default:
			cvFormat = CV_8UC3;
			break;
		}
		return cvFormat;
	}
};

