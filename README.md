# PotholeDetect

In questo progetto è presente la versione del codice in C++ dell'algoritmo presentato nella relazione.


Fare affidamento solo alla classe GPUPotholeAlgorithm poiché è quella maggiormente aggiornata con le ultime versioni.


Dal menù File è possibile aprire o un video o un file.

 - Video: viene aperto un video dove è possibile eseguire la detect
 - File: possibile aprire un file .label (creato mediante applicativo dedicato). In questo file conterà nella prima riga del documento il percorso del video e in automatico lo aprirà.

Non tutti i parametri sono modificabili da GUI. Per modificare il valore di default di un parametro non presente nella gui, visionare il file Algorithm.cpp

 ## Problemi conosciuti

  - Utilizzando il pulsante play (">") si verificano errori nel conteggio delle classificazioni corrette o meno.
	 - si consiglia l'uso del pulsante di esecuzione step by step (">>") oppure modificare la struttura del codice evitando l'utilizzo di un altro thread per ottenere l'immagine dalla camera (o file)


 ## Altri link
 - Applicazione per etichettare video: https://gitlab.com/SimOneGollo/videolabeller.
 - Prime versioni del codice in python (non aggiornato): https://gitlab.com/SimOneGollo/potholepytest.
