#include "Defect.h"

Defect::Defect() {
	this->id = 0;
}
Defect::Defect(vector<Point2f> contours, int id, int padding) {
	this->id = id;
	this->contour = contours;
	this->minbbox = cv::minAreaRect(contours);
	bbox = padding > 0 ? addPadding(minbbox.boundingRect(), padding) : minbbox.boundingRect();
}

Defect::Defect(Rect bbox, int id, int padding) {
	Point2f center = Point2f(bbox.x + bbox.width / 2, bbox.y + bbox.height / 2);
	this->id = id;
	this->minbbox = RotatedRect(center, Size2f(bbox.width, bbox.height), 0);
	this->bbox = padding > 0 ? addPadding(minbbox.boundingRect(), padding) : minbbox.boundingRect();
	contour.push_back(Point2f(bbox.x, bbox.y));
	contour.push_back(Point2f(bbox.x + bbox.width, bbox.y));
	contour.push_back(Point2f(bbox.x + bbox.width, bbox.y + bbox.height));
	contour.push_back(Point2f(bbox.x, bbox.y + bbox.height));

}

static float calculateElongation(Rect rect) 
{
	return rect.width < rect.height ?
		(float)rect.width / rect.height :
		(float)rect.height / rect.width;
}

void Defect::initTracker(InputArray image) {
	cv::TrackerKCF::Params param = cv::TrackerKCF::Params();
	tracker = cv::TrackerKCF::create(param);
	elongation = calculateElongation(bbox);
	tracker->init(image, bbox);
}

bool Defect::trackUpdate(InputArray image) {
	if (tracker == nullptr) {
		return false;
	}
	Rect boxClone = Rect(bbox.x, bbox.y, bbox.width, bbox.height);
	Mat imageClone = image.getMat().clone();

	if (tracker->update(imageClone, bbox)) {
		float newElong = calculateElongation(bbox);
		if (!(newElong > (elongation - 0.05)
			&& newElong < (elongation + 0.05))) {
			return false;
		}
		Rect delta = Rect(Point(boxClone.x - bbox.x, boxClone.y - bbox.y), bbox.size());
		cv::add(contour, Scalar(delta.x, delta.y), contour);
		trackLife++;
		return true;
	}
	return false;
}

bool Defect::operator==(Defect b)
{
	if (this->id != b.id) {
		return false;
	}
}

Rect Defect::addPadding(Rect box, int padding) {
	int sx = box.width + (padding * 2);
	int sy = box.height + (padding * 2);
	int x = box.x - padding;
	int y = box.y - padding;

	return Rect(x, y, sx, sy);
}