#pragma once

#include <qdebug.h>
#include <QThread>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;
using namespace std;

class FrameCapture : public QThread
{
	Q_OBJECT
signals:
	void newImage(Mat image);

public:
	FrameCapture()
		: QThread()
	{
	};
	void open(std::string filename);
	bool grab();
	void skipFrame(int n);
	void goToFrame(int n);
	void setVideoFromFile(std::string filename) { this->videoFile = filename; }
	int getFrameIndex() {
		return cap.get(CAP_PROP_POS_FRAMES);
	};
	std::string getVideoPathName() {
		return this->videoFile;
	}
	std::string getVideoName() {
		QStringList splittedPath = QString::fromStdString(getVideoPathName()).split("/");
		return splittedPath[splittedPath.size()-1].toStdString();
	}

	int width;
	int height;

private:
	void run() override;
	bool internalGrab(Mat& image);
	VideoCapture cap;
	std::string videoFile;
};
