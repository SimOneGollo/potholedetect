#include "PotholeDetect.h"
#include <QtWidgets/QApplication>
#include <opencv2/core.hpp>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qRegisterMetaType<cv::Mat>("Mat");
    qRegisterMetaType<cv::Point>("Point");
    PotholeDetect w;
    w.show();
    return a.exec();
}
